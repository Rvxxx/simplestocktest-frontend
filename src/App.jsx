import React, { useState } from 'react';
import logo from './logo.svg';
import './App.scss';

import Intro from './Components/Intro/Intro';
import Input from './Components/Input/Input';
import Output from './Components/Output/Output';

function App() {

  const [outcome, setOutcome] = useState(null);
  const [message, setMessage] = useState(null);

  return (
    <div className="App">

      <header className="App-header">
        <p>Frontend powered by </p>
        <img src={logo} className="App-logo" alt="logo" />
      </header>

      <main>
        <Intro />
        <Input setOutcome={setOutcome} setMessage={setMessage} />
        <Output outcome={outcome} message={message} />
      </main>

    </div>
  );
}

export default App;
