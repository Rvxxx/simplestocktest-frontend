import React from 'react';
import './Intro.scss';

const Intro = () => {

    return (
        <div className='intro'>

            <p>Welcome to the place to calculate potential profit on the given stock, instructions:</p>
            <ol>
                <li>First provide the stock symbol and press <span>Fetch</span> button... it will fetch the data from the external API (Alpha Vantage page) and upload to the in memory database</li>
                <li>Then provide the dates (from and to) and press <span>Calculate</span> button... it will start the engine to asses the best buying and selling period to maxymise the profit on the stock</li>
                <li>Enjoy the results!</li>
            </ol>

        </div>
    )
}

export default Intro;
