import React, { useState } from 'react';
import './Input.scss';

const Form = ({ setOutcome, setMessage }) => {

    const [symbol, setSymbol] = useState('');

    // TODO: add messegs to the frontend! (such as data were fetched / warn / errors - currently only on the server side)
    const fetchData = () => {
        const symbol = document.getElementById('stockSymbol').value
        setSymbol(symbol)

        fetch('http://localhost:8080/fetch/' + symbol)
            .then(response => response.json())
            .then(result => setMessage(result?.message))
    }

    const getTimePricingProfit = () => {
        const from = document.getElementById('dateFrom').value;
        const to = document.getElementById('dateTo').value;

        fetch('http://localhost:8080/' + symbol + '?from=' + from + '&to=' + to)
            .then(response => response.json())
            .then(result => {
                setOutcome(result);
                setMessage(result?.message);
            })
    }

    return (
        <div className='form'>

            <label form='stockSymbol'>Provide the stock symbol :</label>
            <input id='stockSymbol' type='text' placeholder='stock symbol, ex. IBM' />

            <button id='fetchData' onClick={fetchData}>Fetch</button>

            <div>
                <p>Provide the dates to calculate potential profit :</p>
                <input id='dateFrom' type='text' placeholder='Date from in YYYY-MM-DD' />
                <input id='dateTo' type='text' placeholder='Date to in YYYY-MM-DD' />
            </div>

            <button id='getProfit' onClick={getTimePricingProfit}>Calculate</button>
        </div>
    )
}

export default Form;
