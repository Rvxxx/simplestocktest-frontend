import React from 'react';
import './Output.scss';

const Outcome = ({ outcome, message }) => {

    console.log("in output: " + outcome);

    return (
        <div className='output'>
            <table>
                <tr>
                    <td>Symbol: </td>
                    <td className='makeItBold'>{outcome?.symbol}</td>
                </tr>
                <tr>
                    <td>Best buy date: </td>
                    <td className='makeItBold'>{outcome?.buyDate}</td>
                </tr>
                <tr>
                    <td>Best sell date: </td>
                    <td className='makeItBold'>{outcome?.sellDate}</td>
                </tr>
                <tr>
                    <td>Best buy price: </td>
                    <td className='makeItBold'>{outcome?.buyPrice}</td>
                </tr>
                <tr>
                    <td>Best sell price: </td>
                    <td className='makeItBold'>{outcome?.sellPrice}</td>
                </tr>
                <tr>
                    <td>Gained profit: </td>
                    <td className='makeItBold'>{outcome?.profit}</td>
                </tr>
            </table>
            <div className='message'>
                <p>Messages from the server:</p>
                <p className='fromTheServer'>{message}</p>
            </div>
        </div>
    )
}

export default Outcome;
